<?php

function gtm_admin_settings($form, &$form_state) {
  $form = array();

  $form['gtm_tagid'] = array(
    '#type' => 'textfield',
    '#title' => t('ID'),
    '#default_value' => variable_get('gtm_tagid', 'GTM-XXXXXX'),
    '#description' => t('Your Google Tag Manager ID. Example: GTM-XXXXXX'),
  );

  return system_settings_form($form);
}
